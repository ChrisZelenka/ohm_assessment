from tests import OhmTestCase


class UserTest(OhmTestCase):
    def test_get_multi(self):
        assert self.chuck.get_multi("PHONE") == ['+14086441234', '+14086445678']
        assert self.justin.get_multi("PHONE") == []
    def test_migration(self):	
	assert self.justin.tier == 'Bronze'
	assert self.elvis.point_balance == 1000
