"""update users

Revision ID: dd548ab6a86
Revises: 00000000
Create Date: 2017-10-22 22:25:10.964417

"""

# revision identifiers, used by Alembic.
revision = 'dd548ab6a86'
down_revision = '00000000'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("UPDATE user SET point_balance=1000 WHERE user_id=2")
    op.execute("UPDATE user SET tier='Bronze' WHERE user_id=3")


def downgrade():
    op.execute("UPDATE user SET point_balance=0 WHERE user_id=2")
    op.execute("UPDATE user SET tier='Carbon' WHERE user_id=3")
